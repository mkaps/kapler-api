class Company
  
  include Mongoid::Document
  include Mongoid::Timestamps

# Validations
  validates   :name, :ticker,   presence: true
  validates   :ticker,          uniqueness: true
  validates   :ticker,          length:{ maximum: 4 }

# Fields
  field :ticker,      type: String
  field :name,        type: String
  field :sector,      type: String
  field :industry,    type: String

# Scopes

  scope :by_ticker,       lambda { |ticker| where( ticker: ticker     )   }
  scope :finance_sector,  lambda {          where( sector: "Finance"  )   }



# Figure out a way to dynamically write scopes

  [:technology, :energy].each do |s|
    scope s, -> {where(sector: s) }    
  end



end
