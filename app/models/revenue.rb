class Revenue
  include Mongoid::Document
  include Mongoid::Timestamps

  validates   :ticker, :amount, :year,   presence: true

  field :ticker, type: String  # StockSymbol
  field :source, type: String  # Alphavantage or Intrino (ALVN | INTR)
  field :amount, type: Integer #
  field :year, type: Integer #
  field :yoy, type: Float   # Calculate and store
  field :trend, type: String  # Revenue Trend up or down

  scope :by_ticker, lambda { |ticker| where(ticker: ticker )}
  scope :order_by_year, lambda { order_by(:year => 'desc' )}
  scope :by_year, lambda { |year|   where(year:year )}
  scope :by_year_after_2011, lambda { where(:year.gt => 2011 )}
end
