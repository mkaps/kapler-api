class DataSource # < ActiveModel


  include Mongoid::Document
  include Mongoid::Timestamps


  validates :ds_symbol,   :company_name, :base_url,        presence: true
  validates :ds_symbol,   uniqueness: true
  validates :ds_symbol,   length:{ maximum: 4 }


  field :ds_symbol,       type: String
  field :company_name,    type: String
  field :company_url,     type: String
  field :source_type,     type: String
  field :access_key,      type: String
  field :base_url,        type: String
  field :username,        type: String # Mongoid::EncryptedString  -  Implement this later
  field :password,        type: String # Mongoid::EncryptedString  -  Implement this later
end
