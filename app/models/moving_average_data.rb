class MovingAverageData
  
  include Mongoid::Document
  include Mongoid::Timestamps

# Fields
  field :ticker,                      type: String 
  field :date,                        type: String  # last calculated date
  field :ma_200_50_signal_date,       type: String  # When it crossed first time
  field :moving_average_data_exists,  type: Boolean # True when all data necessary available
  field :ma_200_50_signal,            type: Boolean # When it matches the signal
  
# Cannot implement this for now as there are issues with implmenting helper methods inside Thor modules
  # field :price_vs_50,                 type: Boolean # Closed price vs 50-day MA
  # field :potential_by_52w_high,       type: Float   # Upside potential based on 52W high
  # # field :stop_loss_price,             type: Float   # When to sell signal if it is going down
  
# Scopes
  scope :by_ticker,       lambda { |ticker|       where(ticker: ticker)           }
  scope :find_by_date,    lambda { |date|         where(date: date)               }
  scope :order_by_date,   lambda {                order_by(:date => 'desc')       }
           
end
