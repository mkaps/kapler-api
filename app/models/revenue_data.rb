class RevenueData

  include Mongoid::Document
  include Mongoid::Timestamps

  validates   :ticker,           presence: true

# Fields
  field :ticker, type: String
  field :company_exists, type: Boolean # True when ticker exists in company collection
  field :required_years, type: Boolean # True when all data necessary available
  field :yoy_signal, type: Boolean # YOY over 20
  field :trend_signal, type: Boolean # Revenue Trend up

# Scopes
  scope :by_ticker, lambda { |ticker| where(ticker: ticker)}

end
