class Aggregate

  include Mongoid::Document
  include Mongoid::Timestamps

  validates  :name, :count, presence: true
  validates  :name, uniqueness: true

  field :type, type: String
  field :name, type: String
  field :count, type: Integer
  field :date, type: Date

  scope :companies_by_sector, -> { where(type: "sector").only(:name, :count)}
  
end
