class MovingAverage
  include Mongoid::Document
  include Mongoid::Timestamps

  validates   :ticker, :indicator, :interval, :price, :date,   presence: true

  field :ticker,      type: String # symbol
  field :indicator,   type: String # simple
  field :interval,    type: String # daily
  field :date,        type: String # date
  field :series_type, type: String # "200-day" or "50-day" 
  field :open_close,  type: String # Close or Open
  field :price,       type: Float  # price
  field :source,      type: String # Alphavantage or Intrino (ALVN | INTR)

  scope :by_ticker,       lambda { |ticker|       where(ticker: ticker)           }
  scope :by_series_type,  lambda { |series_type|  where(series_type: series_type) }
  scope :order_by_date,   lambda {                order_by(:date => 'desc')       }
  scope :find_by_date,    lambda { |date|         where(date: date)               }
end
