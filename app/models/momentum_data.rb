class MomentumData
  include Mongoid::Document
  include Mongoid::Timestamps

  # validates   :ticker, :indicator, :interval, :price, :date,   presence: true

  field :ticker, type: String                 # symbol
  field :revenue_signal, type: Boolean        # simple
  field :moving_average_signal, type: Boolean # daily
  field :date, type: String                   # date
  
  scope :by_ticker,       lambda { |ticker|       where(ticker: ticker)           }
  scope :order_by_date,   lambda {                order_by(:date => 'desc')       }
  scope :find_by_date,    lambda { |date|         where(date: date)               }
end
