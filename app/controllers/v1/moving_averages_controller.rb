module V1
	class MovingAveragesController < ApplicationController

    def index
      sma     =   MovingAverage.distinct(:ticker).sort
      months  =   MovingAverage.distinct(:date).map{|ma| ma.split('-')[1]}.uniq
      years   =   MovingAverage.distinct(:date).map{|ma| ma.split('-')[0]}.uniq
      count   =   sma.count
      render json: {count: count, symbols: sma, years: years, months: months}
    end

    def create
      moving_average =  MovingAverage.new(moving_average_params)

      if moving_average.save
        render json: {status: 'SUCCESS', message:'Added moving_average', data: moving_average }, status: :ok
      else
        render json: {status: 'ERROR', message:'MovingAverage not saved', data: moving_average.errors }, status: :unprocessable_entity
      end
    end

    def show
      if params['series_type']
        sma = MovingAverage.by_ticker(params[:id]).by_series_type(params['series_type']).order_by_date
      else
        sma = MovingAverage.by_ticker(params[:id]).order_by_date
      end
      render json: { data: sma }
    end

    private

    def moving_average_params
        params.permit(:ticker, :indicator, :interval, :price, :date)
    end
	end
end
