module V1
	class RevenueDataController < ApplicationController
	  def index
	    rds               =   RevenueData.all
	    count             =   RevenueData.distinct(:ticker).count
	    tickers           =   rds.map(&:ticker).sort

	    render json: { count: count, symbols: tickers, data: rds }
	  end

	  def new
	    @rd = RevenueData.new
	  end

	  def create
	    rd =   RevenueData.new(revenue_data_params)

	    if rd.save
	      render json: {status: 'SUCCESS', message:'Added revenue', data: rd }, status: :ok
	    else
	      render json: {status: 'ERROR', message:'Revenue not saved', data: rd.errors }, status: :unprocessable_entity
	    end
	  end

	  def show
	    rev_data = RevenueData.by_ticker(params[:id])
	    render json: rev_data
	  end


	  private

	  def revenue_data_params
	      params.permit(:ticker, :company_exists, :required_years, :yoy_signal, :trend_signal)

	  end
	end
end
