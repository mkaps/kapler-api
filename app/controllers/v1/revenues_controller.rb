module V1
	class RevenuesController < ApplicationController
	  def index
	    @unique_tickers   =   Revenue.distinct(:ticker).sort
	    count             =   @unique_tickers.count
	    years             =   Revenue.distinct(:year).sort
	    puts years.inspect
	    render json: { count: count, symbols: @unique_tickers, years: years }
	  end

	  def create
	    revenue =   Revenue.new(revenue_params)

	    if revenue.save
	      render json: {status: 'SUCCESS', message:'Added revenue', data: revenue }, status: :ok
	    else
	      render json: {status: 'ERROR', message:'Revenue not saved', data: revenue.errors }, status: :unprocessable_entity
	    end
	  end

	  def show
	    rev = Revenue.where(ticker: params[:id]).where(:year.gt => 2011).order_by_year
	    render json: rev
	  end

	private

		def revenue_params
	    params.permit(:ticker, :source, :amount, :year)
		end
	end
end
