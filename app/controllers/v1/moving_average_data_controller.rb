module V1
	class MovingAverageDataController < ApplicationController

	  def index
	    mads                =     MovingAverageData.all
	    tickers             =     mads.map(&:ticker).sort.uniq
	    tickers_count       =     tickers.count
	    tickers_record_cnt  =     {}
	    tickers.each do |ticker|
	      record_cnt =  MovingAverageData.by_ticker(ticker).count
	      tickers_record_cnt[ticker] = record_cnt
	    end
	    metadata = {}
	    metadata['tickers_by_total'] =  tickers_record_cnt
	    render json: { count: tickers_count, metadata: metadata }
	  end

	  def new
	    @mad = MovingAverageData.new
	  end

	  def create
	    mad =   MovingAverageData.new(revenue_data_params)

	    if mad.save
	      render json: {status: 'SUCCESS', message:'Added revenue', data: mad }, status: :ok
	    else
	      render json: {status: 'ERROR', message:'MovingAverage not saved', data: rd.errors }, status: :unprocessable_entity
	    end
	  end

	  def show
	    mads                =     MovingAverageData.by_ticker(params[:id]).order_by_date
	    total               =     mads.count
	    months              =     mads.map(&:date).map{|d| d.split("-")[1]}.uniq
	    months_cnt          =     {}
	    months.each do |m|
	      cnt_200_day = 0
	      mads.each{|i| cnt_200_day += 1 if i['date'].split("-")[1] == m}
	      months_cnt[m.to_i] = cnt_200_day
	    end
	    metadata = {}
	    metadata['total']     =   total
	    metadata['by_month']  =   months_cnt

	    render json: { metadata: metadata, data: mads }
	  end


	  private

	  def revenue_data_params
	      params.permit(:ticker, :company_exists, :required_years, :yoy_signal, :trend_signal)

	  end
	end
end
