module V1
    require 'data_source'

    class SourcesController < ApplicationController

      def index
        sources = DataSource.order('created_at DESC')

        # render json: { status: 'SUCCESS', message: 'All Sources', data: sources }, status: :ok
        render json: { data: sources }, status: :ok
      end

      def show
        source = DataSource.where(ds_symbol: params[:id])

        render json: { status: 'SUCCESS', message: 'All Sources', data: source }
      end

      def create
        source =  DataSource.new(source_params)

        if source.save
          render json: {status: 'SUCCESS', message:'Added source', data:source},status: :ok
        else
          render json: {status: 'ERROR', message:'Source not saved', data:source.errors},status: :unprocessable_entity
        end

        # render json: source, status: :created
      end

      def destroy
        source = DataSource.where(ds_symbol: params[:id])
        source.destroy

        render json: {status: 'SUCCESS', message:'Deleted Data Source', data:source}

      end

      private

      def source_params
        params.permit(:ds_symbol, :company_name, :company_url,:base_url, :access_key, :source_type)
      end

    end
end
