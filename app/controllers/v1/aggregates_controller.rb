module V1

  class AggregatesController < ApplicationController

    def index
      @companies_by_sector =  Aggregate.companies_by_sector
      @companies_by_industry =   Aggregate.where(type: "industry").only(:name, :count)

      render json:  {
                      companies_by_sector: @companies_by_sector,
                      companies_by_industry:  @companies_by_industry
                    }
    end

    def create
        aggregate =  Aggregate.new(aggregate_params)

        if aggregate.save
          render json: {status: 'SUCCESS' }, status: :ok
        else
          render json: {status: 'Error' }, status: :unprocessable_entity
        end
    end


    private

    def aggregate_paras
        params.permit(:type, :name, :count, :date)
    end

  end

end
