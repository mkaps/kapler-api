module V1

    class CompaniesController < ApplicationController
      # include AlvnHelper
      # include IntrHelper

      # skip_before_action :verify_authenticity_token

      def index
        count     =   Company.all.count
        companies =   Company.all.limit(10).only(:name, :ticker)
        render json:  {total: count, companies: companies}
      end


      def show
        company = Company.where(ticker: params[:id]).only(:name, :ticker, :sector, :industry)
        render json: company
      end

      def create

        company =  Company.new(company_params)

        if company.save
          render json: {status: 'SUCCESS', message:'Added company', data: company }, status: :ok
          puts "CT : #{company.ticker}"

          # process_company(company.ticker)
        else
          render json: {status: 'ERROR', message:'Company not saved', data: company.errors }, status: :unprocessable_entity
        end
        # process_company_data(company.ticker)
      end



      def destroy
        comapny = Company.where(ticker: params[:id])
        comapny.destroy

        render json: {status: 'SUCCESS', message:'Deleted Data Source', data:comapny}, status: :ok
      end

      private

      def company_params
        params.permit(:ticker, :name, :sector, :industry)
      end

      def process_company_data(ticker)
          helpers.populate_revenue(ticker)
          helpers.get_historical_data(ticker, 200, 11, 2017)
          helpers.populate_yoy_revenues(ticker)
          helpers.get_historical_data(ticker, 50, 11, 2017)
          # helpers.get_historical_data(ticker, 50, 10, 2017)
          # helpers.get_historical_data(ticker, 50, 8, 2017)
      end
    end
end
