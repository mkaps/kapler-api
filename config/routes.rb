Rails.application.routes.draw do

# Root
  root to: 'v1/aggregates#index'

  namespace 'v1' do
    resources :sources, :companies #, :only [:show, :index]

# Aggregates
    get  "aggregates"                   =>    "aggregates#index"

# Momentum Data
    get  "momentum_data/new"      =>    "momentum_data#new"
    get  "momentum_data/:id"      =>    "momentum_data#show"
    get  "momentum_data"          =>    "momentum_data#index"
    post "momentum_data/"         =>    "momentum_data#create"

# Moving Average Data
    get  "moving_average_data/new"      =>    "moving_average_data#new"
    get  "moving_average_data/:id"      =>    "moving_average_data#show"
    get  "moving_average_data"          =>    "moving_average_data#index"
    post "moving_average_data/"         =>    "moving_average_data#create"

# Moving Averages
    get  "moving_averages/simple"       =>    "moving_averages#index"
    get  "moving_averages/new"          =>    "moving_averages#new"
    get  "moving_averages/simple/:id"   =>    "moving_averages#show"
    post "moving_averages/"             =>    "moving_averages#create"

# Revenue
      get  "revenue_data"                 =>    "revenue_data#index"
      get  "revenue_data/new"             =>    "revenue_data#new"
      get  "revenue_data/:id"             =>    "revenue_data#show"
      post "revenue_data/"                =>    "revenue_data#creatse"

# Revenues
      get  "revenues/new"                 =>    "revenues#new"
      get  "revenues/:id"                 =>    "revenues#show"
      post "revenues/"                    =>    "revenues#create"
      get  "revenues"                     =>    "revenues#index"
    end
end
